import { Button } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import { NavLink } from 'react-router-dom';
import './productAds.css'

export default function ProductAds() {
  return (
    <div className='containerProductAds'>
      <Card>
        <Card.Img variant="top" src="https://images.pexels.com/photos/1578384/pexels-photo-1578384.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" />
        <Card.Body className='cardBody'>
          <Card.Text className='TitleText'>
            COMFORT IN EVERY STEP
          </Card.Text>
          <Card.Text className='textInformation'>
            Cause everyone should know the feeling of running in that perfect pair.
          </Card.Text>
          <Button variant="dark" as={NavLink} to="/products">Find your Shoe</Button>
        </Card.Body>
      </Card>
    </div>
  );
}